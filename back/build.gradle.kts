import org.jetbrains.kotlin.noarg.gradle.NoArgExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    id("org.springframework.boot") version "2.1.4.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    id("com.github.johnrengelman.shadow") version "6.1.0"
    id ("org.jetbrains.kotlin.plugin.noarg") version "1.4.20"

    kotlin("jvm") version "1.4.20"
    kotlin("plugin.spring") version "1.4.20"
    kotlin("plugin.allopen") version "1.4.20"
    //kotlin("plugin.jpa") version "1.4.20"
    kotlin("kapt") version "1.4.20"
}

group = "com.datacloudcrime"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8
// fixme spring boot 2.3 breaking change
val developmentOnly: Configuration by configurations.creating

configurations {
    runtimeClasspath {
        extendsFrom(developmentOnly)
    }
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}
//configurations {
//    compileOnly {
//        extendsFrom(configurations.annotationProcessor.get())
//    }
//}

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation("org.apache.hadoop:hadoop-common:2.2.0") {
        exclude(group="javax.servlet", module="servlet-api")
        exclude(module="slf4j-log4j12")
        exclude(module="logback-classic")
        exclude(module="log4j-slf4j-impl")
        exclude(group="org.eclipse.jetty.aggregate", module = "jetty-all")
    }
    implementation("org.apache.hive:hive-jdbc:1.2.1") {
        exclude(group="javax.servlet", module="servlet-api")
        exclude(module="slf4j-log4j12")
        exclude(module="logback-classic")
        exclude(module="log4j-slf4j-impl")
        exclude(group="org.eclipse.jetty.aggregate", module = "jetty-all")
    }
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.apache.tomcat:tomcat-jdbc")
    //implementation("org.springframework.data:spring-data-hadoop:2.5.0.RELEASE")

    //mayberemove
    //implementation("com.alibaba:druid-spring-boot-starter:1.1.10")
    //mayberemove
    implementation("org.springframework.boot:spring-boot-starter-data-jdbc")

    implementation("org.springframework.boot:spring-boot-starter-web")


    //implementation("javax.servlet:javax.servlet-api:4.0.1")
    //developmentOnly("org.springframework.boot:spring-boot-devtools")
    //runtimeOnly("org.springframework.boot:spring-boot-devtools")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")


    //implementation("org.springframework.boot:spring-boot-starter-integration")


    //annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    //implementation("org.springframework.boot:spring-boot-configuration-processor")
    kapt("org.springframework.boot:spring-boot-configuration-processor")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
tasks {
    named<ShadowJar>("shadowJar") {
        archiveBaseName.set("shadow")
        mergeServiceFiles()
        isZip64 = true // needed for hive-jdbc, do not enable if not needed
        manifest {
            attributes(mapOf("Main-Class" to "com.datacloudcrime.datacloudcrime.DatacloudcrimeApplicationKt"))
        }
    }
}

tasks {
    build {
        dependsOn(shadowJar)
    }
}
configure<NoArgExtension> {
    invokeInitializers = true
    annotation("org.springframework.data.relational.core.mapping.Table")
}