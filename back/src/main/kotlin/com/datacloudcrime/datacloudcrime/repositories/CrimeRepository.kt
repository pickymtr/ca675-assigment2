package com.datacloudcrime.datacloudcrime.repositories

import com.datacloudcrime.datacloudcrime.controllers.NumberCrimeByCity
import com.datacloudcrime.datacloudcrime.controllers.NumberCrimeByCityAndMonth
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.BeanPropertyRowMapper
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.queryForObject
import org.springframework.stereotype.Repository
import java.beans.BeanProperty
import java.util.*

@Repository
class CrimeRepository {
    @Autowired
    lateinit var jdbcTemplate: JdbcTemplate

    @Autowired
    lateinit var namedParameterJdbcTemplate: NamedParameterJdbcTemplate

    fun getCrimeByCityAndMonth(areaName: String, month: String): MutableList<MutableMap<String, Any>> {
        val args = arrayOf(areaName, month)
        val ret = jdbcTemplate.queryForList(
            """
                SELECT 
                    areaname, 
                    dateoccured
                FROM 
                    crime_losangeles 
                WHERE 
                    areaname = ? 
                    AND 
                    substring(dateoccured, 1, 2) = ? 
                LIMIT 1000
                """, *args
        )
        println(ret)
        return ret
    }

    private fun getMonthString(month: Int): String {
        var ret = month.toString()
        if (month < 10) {
            ret = "0$ret"
        }
        return ret;
    }

    fun getNbCrimeByCityForAllMonth(areaName: String): List<Triple<String, String, Int>> {
        val array = (1..12).map {
            Triple(
                areaName,
                this.getMonthString(it),
                this.getNbCrimeByCityAndMonth(areaName, getMonthString(it))
            )
        }
        println(array)
        return array
    }

    fun getNbCrimeByCityAndMonth(areaName: String, month: String): Int {
        val args = arrayOf(areaName, month)
        val ret = jdbcTemplate.queryForObject(
            """
                SELECT  COUNT(*)
                FROM    crime_losangeles
                WHERE   areaname = ?
                        AND
                        substring(dateoccured, 1, 2) = ? 
                LIMIT 1000
            """, args, Int::class.java
        )
        println(ret)
        return ret
    }

    fun postNbCrimeByCityAndMonth(areaName: String, month: String, numberCrime: Int) {
        val args = arrayOf(areaName, month, numberCrime)
        val ret = jdbcTemplate.update(
            """
            INSERT INTO number_crime_by_months_and_city VALUES(?, ?, ?)
            """, *args
        )
    }

    fun getAllAreaPlace(): List<String>? {
        return jdbcTemplate.queryForList(
            """
                SELECT DISTINCT areaname
                FROM crime_losangeles
            """, String::class.java
        )
    }

    fun getNbCrimeGroupedByByAreaPlaceAndMonth(city: String): List<NumberCrimeByCityAndMonth> {
        println(city)
        var parameters = MapSqlParameterSource()
        parameters.addValue("city", city)
        var ret = jdbcTemplate.query(
            """
                SELECT *
                FROM number_crime_by_months_and_city
                WHERE city = ?
        """, arrayOf(city)
        ) { rs, _ ->
            NumberCrimeByCityAndMonth(
                city = rs.getString("city"),
                month = rs.getString("month"),
                numberOfCrime = rs.getInt("numberofcrime")
            )
        }
        println(ret)
        return ret
    }

    //fun getCountForAllCities(): List<NumberCrimeByCity> {
    //    var ret = jdbcTemplate.queryForList(
    //        """
    //            SELECT DISTINCT city
    //            FROM  number_crime_by_months_and_city
    //        """, String::class.java
    //    ).map {
    //        jdbcTemplate.query(
    //            """
    //            SELECT *
    //            FROM number_crime_by_months_and_city
    //            WHERE city = ?
    //            """, arrayOf(it)
    //        ) { rs, _ ->
    //            NumberCrimeByCityAndMonth(
    //                city = rs.getString("city"),
    //                month = rs.getString("month"),
    //                numberOfCrime = rs.getInt("numberofcrime")
    //            )
    //        }
    //    }
    //    ret.forEach {

    //    }
    //    println(ret)
    //    return ret
    //}
}