package com.datacloudcrime.datacloudcrime

import com.datacloudcrime.datacloudcrime.services.CrimeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Component

@Component
class InitClass() {
    @Autowired lateinit var jdbcTemplate: JdbcTemplate
    @Autowired lateinit var crimeService: CrimeService

    @EventListener
    fun init(event: ContextRefreshedEvent) {
        jdbcTemplate.execute(
            """CREATE TABLE IF NOT EXISTS crime_losangeles(DrNumber String, DateReported String, DateOccured String, TimeOccured String, AreaId String, AreaName String) COMMENT 'crime daatasets' ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde' WITH SERDEPROPERTIES ("separatorChar" = ",") STORED AS TEXTFILE tblproperties('skip.header.line.count'='1') """
        );
        val nbRows = jdbcTemplate.queryForObject(
                """
                    SELECT  COUNT(*)
                    FROM    crime_losangeles
                    LIMIT 1
                """, Int::class.java
        )!!
        if (nbRows < 1) {
            jdbcTemplate.execute("""LOAD DATA INPATH '/tmp/crimeDataSets/Crime_Data_from_2010.csv' INTO TABLE crime_losangeles""")
            println("crime losangeles and its data loaded")
            jdbcTemplate.execute(
                """CREATE TABLE IF NOT EXISTS number_crime_by_months_and_city(City String, Month String, NumberOfCrime Int)"""
            );
            this.crimeService.getAllAreaPlace()?.forEach {
                this.crimeService.getNbCrimeByCityForAllMonth(it).forEach {
                    this.crimeService.postNbCrimeByCityAndMonth(it.first, it.second, it.third)
                }
            } ?: throw RuntimeException("yow error")
            println("number_crime_by_mount created and its data loaded")
        }
    }
}

@SpringBootApplication
class DatacloudcrimeApplication

fun main(args: Array<String>) {
    runApplication<DatacloudcrimeApplication>(*args)
}

