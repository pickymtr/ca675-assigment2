package com.datacloudcrime.datacloudcrime.controllers

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonView
import com.datacloudcrime.datacloudcrime.services.CrimeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.web.bind.annotation.*

@Table("number_crime_by_months_and_city")
data class NumberCrimeByCityAndMonth(
    @Column("city")
    val city: String,
    @Column("month")
    val month: String,
    @Column("numberofcrime")
    val numberOfCrime: Int) {}

data class NumberCrimeByCity(
    val city: String,
    val numberOfCrime: Int) {}

@RestController
@RequestMapping("/crimes")
class CrimeController(@Autowired val crimeService: CrimeService) {

    @GetMapping
    fun get(@RequestParam areaName: String, @RequestParam month: String): String {
        println("je print area name $areaName")
        println("je print month $month")
        return this.crimeService.getCrimesByMonth(areaName, month).toString()
    }

    @CrossOrigin(origins = ["http://localhost:8080", "http://localhost:3000"])
    @GetMapping("/countGroupedByMonthAndCity")
    fun getCountGroupedByMonthsAndCity(@RequestParam areaName: String): List<NumberCrimeByCityAndMonth> {
        println(areaName)
        return this.crimeService.getNbCrimeGroupedByByAreaPlaceAndMonth(areaName)
    }

    ////@GetMapping("/countForAllCities")
    //fun getCountForAllCities(): List<NumberCrimeByCity> {
    //    return this.crimeService.
    //}
}