package com.datacloudcrime.datacloudcrime.config
import org.apache.tomcat.jdbc.pool.DataSource
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.core.JdbcTemplate

@Configuration
class HiveJdbcConfig {
    @Value("\${hive.url}")
    private val url: String? = null

    @Value("\${hive.driver}")
    private val driver: String? = null

    @Value("\${hive.user}")
    private val user: String? = null

    @Value("\${hive.password}")
    private val password: String? =null

    @Bean
    fun dataSource(): DataSource {
        val dataSource = DataSource()
        dataSource.url = url
        dataSource.driverClassName = driver
        dataSource.username = user
        dataSource.password = password
        return dataSource
    }

    @Bean
    fun jdbcTemplate(dataSource: DataSource?): JdbcTemplate {
        return JdbcTemplate(dataSource!!)
    }
}
