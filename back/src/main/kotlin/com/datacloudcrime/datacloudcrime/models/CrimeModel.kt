package com.datacloudcrime.datacloudcrime.models

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonView
import com.rebirthCorp.rebirth.utils.generateUUID
import org.springframework.data.annotation.Id
//import org.springframework.data.annotation.Id
//import org.springframework.data.relational.core.mapping.Table
import java.io.Serializable
import java.sql.Timestamp
import java.time.Instant
//import javax.jdo.annotations.Column
import javax.validation.constraints.*

//@Table("question_post27")
class CrimeModel(
    //@Column(name = "score")
    @JsonView(SummaryView::class)
    var score: Int = 0
) : Cloneable {
    @Id
    @JsonView(SummaryView::class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    var id: String = generateUUID()

    interface SummaryView
    interface DetailedView : SummaryView
    interface PrivateView : DetailedView
}