package com.datacloudcrime.datacloudcrime.services

import com.datacloudcrime.datacloudcrime.controllers.NumberCrimeByCity
import com.datacloudcrime.datacloudcrime.controllers.NumberCrimeByCityAndMonth
import com.datacloudcrime.datacloudcrime.repositories.CrimeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Service

@Service
class CrimeService() {
    @Autowired lateinit var repository: CrimeRepository
    @Autowired lateinit var jdbcTemplate: JdbcTemplate

    fun getCrimesByMonth(areaName: String, month: String): MutableList<MutableMap<String, Any>> {
        val rows = jdbcTemplate.queryForList("show tables");
        return this.repository.getCrimeByCityAndMonth(areaName, month)
    }
    fun getNbCrimeByCityForAllMonth(areaName: String): List<Triple<String, String, Int>> {
        val res = this.repository.getNbCrimeByCityForAllMonth(areaName)
        return res
    }
    fun postNbCrimeByCityAndMonth(areaName: String, month: String, numberCrime: Int) {
        return this.repository.postNbCrimeByCityAndMonth(areaName, month, numberCrime)
    }
    fun getNbCrimeGroupedByByAreaPlaceAndMonth(city: String):List<NumberCrimeByCityAndMonth> {
        return this.repository.getNbCrimeGroupedByByAreaPlaceAndMonth(city)
    }
    //fun getCountForAllCities(): List<NumberCrimeByCity> {
    //    return this.repository.getCountForAllCities();
    //}
    fun getAllAreaPlace(): List<String>? {
        return this.repository.getAllAreaPlace()
    }
}
