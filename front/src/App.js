import './App.css';
import React from 'react';
import Highcharts from 'highcharts';
import {
  HighchartsChart, Chart, withHighcharts, XAxis, YAxis, Title, Subtitle, Legend, LineSeries, Caption
} from 'react-jsx-highcharts';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      xAxis : {
        type: 'category'
      },
      numberOfMonths: 12,
      months: [
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
      ],
      areas: ['77th Street' , 'West Valley', 'N Hollywood', 'Devonshire', 'Hollenbeck', 'Southeast', 'Southwest', 'Hollywood', 'Northeast',
      'Van Nuys', 'Foothill', 'Wilshire', 'Central', 'Rampart', 'West LA', 'Pacific', 'Topanga', 'Mission', 'Olympic', 'Harbor',
      'Newton'],
      results: []
    };
  }
  

  componentWillMount = () => {
    this.getCrimes();
  }


  getCrimesByArea =  async (areaName) => {
    var crimes = await fetch(`http://localhost:8080/crimes/countGroupedByMonthAndCity?areaName=${areaName}`)
    .then(response => response.json())
    .then(
      data => {
        console.log(data)
        return data.map(obj => obj.numberOfCrime);
      }, error => {
        console.log(error)
        return 0;
      }
    );
    //console.log(crimes);
    return crimes;
  
  }

  getCrimes = async () => {
    var res = [];
    var i;
    for (i in this.state.areas) {
      console.log(this.state.areas[i])
      var crimes = await this.getCrimesByArea(this.state.areas[i]);
      res.push(<LineSeries name={this.state.areas[i]} data={crimes} />);
    }
    // var res = await Promise.all(this.state.areas.map(async (item, index) => {
    //   var crimes = await this.getCrimesByArea(item);
    //   return 
    // }));
    console.log("result", res)
    this.setState({results: res});
  }

  render() {
    return (
      <div className="app">
      <HighchartsChart >
        <Chart />

        <Title>Number of crimes by by month depending on area</Title>

        {/* <Subtitle>Source: thesolarfoundation.com</Subtitle> */}

        <Legend layout="vertical" align="right" verticalAlign="middle" />

        <XAxis categories={this.state.months}>
          <XAxis.Title>Month</XAxis.Title>
        </XAxis>

        <YAxis>
          <YAxis.Title>Number of crimes</YAxis.Title>
          {this.state.results}
          {/* {this.getCrimes()} */}
        </YAxis>
        {/* <Caption align="center">The installation sector sees the most growth.</Caption> */}
      </HighchartsChart>
    </div>
    );
  }
}

export default withHighcharts(App, Highcharts);
